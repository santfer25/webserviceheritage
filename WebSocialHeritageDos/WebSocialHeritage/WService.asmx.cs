﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using WebSocialHeritage.Clases;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;


namespace WebSocialHeritage
{
    /// <summary>
    /// Descripción breve de WebService1
    /// </summary>
    [WebService(Namespace = "http://socialheritage/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        //const string ruta = "server=mysql14337-env-2222075.j.facilcloud.com; database=socialheritage; Uid=root; pwd=DYCcva86319;";
        const string ruta = "server=172.16.242.92; database=socialheritage; Uid=root; pwd=toor;";



        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void GetMonenPuntos()
        {

           
            List<Monumentos> listMonumentos = new List<Monumentos>();

            var json = "";
            MySqlConnection conexion = new MySqlConnection(ruta);
            conexion.Open();

            MySqlCommand command = conexion.CreateCommand();
            command.CommandText = ("SELECT p.idPun_Patr, p.lat, p.long, p.url_img_punto, p.nombre, p.descripcion  FROM punto_patrimonial p");
            command.Connection = conexion;

            MySqlDataReader reader = command.ExecuteReader();


            while (reader.Read())
            {
                Monumentos mou = new Monumentos();
                
                mou.Idmonumento = reader.GetInt32(0);
                mou.Latitud = reader.GetDecimal(1);
                mou.Longitud = reader.GetDecimal(2);
                mou.Url_img = reader.GetString(3);
                mou.Nombre = reader.GetString(4);
                mou.Descripcion = reader.GetString(5);


               
                

                listMonumentos.Add(mou);
            }

            command.Connection.Close();

            //json = jss.Serialize(listMonumentos);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            JavaScriptSerializer jss = new JavaScriptSerializer();
            Context.Response.Write(jss.Serialize(listMonumentos));
           // return json;
        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void GetContVideosAudiosPunto(int idpunto, int idtipocont)
        {

            List<ContenidoPunto> listaContenidos = new List<ContenidoPunto>();
            MySqlConnection conexion = new MySqlConnection(ruta);
            conexion.Open();

            MySqlCommand command = conexion.CreateCommand();
            command.CommandText = ("SELECT c.idContenido, c.idPun_Patr, c.url, c.nombre, c.descripcion  FROM contenido c WHERE c.idTipoCont = " + idtipocont + " and c.idPun_Patr = " + idpunto);
            command.Connection = conexion;

            MySqlDataReader reader = command.ExecuteReader();


            while (reader.Read())
            {
                ContenidoPunto contenido = new ContenidoPunto();


                contenido.PuntoMonumento = reader.GetUInt16(1);
                contenido.Url_contenido = reader.GetString(2);
                contenido.Nombre = reader.GetString(3);
                contenido.Descripcion = reader.GetString(4);


                listaContenidos.Add(contenido);
            }

            command.Connection.Close();

            //json = jss.Serialize(listMonumentos);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            JavaScriptSerializer jss = new JavaScriptSerializer();
            Context.Response.Write(jss.Serialize(listaContenidos));

        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void GetContenidosImagenesDePunto(int idpunto)
        {

            List<ContenidoPunto> listaContenidos = new List<ContenidoPunto>();
            MySqlConnection conexion = new MySqlConnection(ruta);
            conexion.Open();

            MySqlCommand command = conexion.CreateCommand();
            command.CommandText = ("SELECT c.idContenido, c.idPun_Patr, c.url, c.nombre  FROM contenido c WHERE c.idTipoCont = 1 and c.idPun_Patr = " + idpunto);
            command.Connection = conexion;

            MySqlDataReader reader = command.ExecuteReader();


            while (reader.Read())
            {
                ContenidoPunto contenido = new ContenidoPunto();


                contenido.PuntoMonumento = reader.GetUInt16(1);
                contenido.Url_contenido = reader.GetString(2);
                contenido.Nombre = reader.GetString(3);

                listaContenidos.Add(contenido);
            }

            command.Connection.Close();

            //json = jss.Serialize(listMonumentos);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            JavaScriptSerializer jss = new JavaScriptSerializer();
            Context.Response.Write(jss.Serialize(listaContenidos));

        }




        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void InsertPunto(String descrip, String imgbase64, String lati, String longitud, String url, String nom)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            String PathServerAppSocial = "http://186.159.114.39/imagenes/";
            String resul = "";
            // Convert base 64 string to byte[]
            byte[] imageBytes = Convert.FromBase64String(imgbase64);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);

            Random r = new Random(DateTime.Now.Millisecond);
            int nameRandomForNow = r.Next(10000, 99999);

            var result = "n";

           
            MySqlConnection conexion = new MySqlConnection(ruta);
            conexion.Open();

            MySqlCommand command = conexion.CreateCommand();
            

            try
            {

                String filepath = "C:/inetpub/wwwroot/WebSiteHeritage/imagenes/" + nameRandomForNow + ".Jpeg";
                image.Save(filepath, ImageFormat.Jpeg);

                String sentenci = "INSERT INTO punto_patrimonial ( `descripcion`, `lat`, `long`, `url_img_punto`, `nombre`) " +
                   "VALUES ('" + descrip + "', " + lati + ", " + longitud + ", '" + PathServerAppSocial + nameRandomForNow + ".Jpeg" + "', '" + nom + "');";


                command.CommandText = (sentenci);
                command.Connection = conexion;
                command.ExecuteNonQuery();


               Context.Response.Write(result = "Insert Exito");
                command.Connection.Close();

            }
            catch (Exception e)
            {

                Context.Response.Write(result = "Error al Crear Monumento");
            }

            //Context.Response.Write(result = "Monumento Creado");
            //command.Connection.Close();

        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void CargarImag(String imgbase64, int idtipocont, int idpuntopatr, String descripcion)
        {
            String PathServerAppSocial = "http://186.159.114.39/imagenes/";
            String resul = "";
            // Convert base 64 string to byte[]
            byte[] imageBytes = Convert.FromBase64String(imgbase64);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);

            MySqlConnection conexion = new MySqlConnection(ruta);
            conexion.Open();

            MySqlCommand command = conexion.CreateCommand();
            

            Random r = new Random(DateTime.Now.Millisecond);
            int nameRandomForNow = r.Next(10000, 99999);
            try
            {
                String filepath = "C:/inetpub/wwwroot/WebSiteHeritage/imagenes/" + nameRandomForNow + ".Jpeg";
                image.Save(filepath, ImageFormat.Jpeg);

                command.CommandText = ("INSERT INTO contenido (`idTipoCont`, `idPun_Patr`, `url`, `nombre`, `descripcion`) " +
                                                    "VALUES (" + idtipocont + ", " + idpuntopatr + ", '" + PathServerAppSocial + nameRandomForNow + ".Jpeg" + "', '" + nameRandomForNow + "', '" + descripcion + "');");
                command.Connection = conexion;

                command.ExecuteNonQuery();

                resul = "Imagen Guardada";
            }
            catch (Exception ex)
            {
                
                resul = "Error al Guardar" + ex.GetBaseException();
            }

            command.Connection.Close();


            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(resul);

        }



        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void ValidarUsuario(string user, string pass)
        {

            int validUser = 0;
            MySqlConnection conexion = new MySqlConnection(ruta);
            conexion.Open();

            MySqlCommand command = conexion.CreateCommand();
            command.CommandText = ("select idusuarios from usuarios usu where usu.user = '" + user + "' AND usu.password = '" + pass + "'");
            command.Connection = conexion;

            MySqlDataReader reader = command.ExecuteReader();


            while (reader.Read())
            {
                validUser = 1;
            }

            command.Connection.Close();

            //json = jss.Serialize(listMonumentos);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            JavaScriptSerializer jss = new JavaScriptSerializer();
            Context.Response.Write(jss.Serialize(validUser));

        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void AgregarUsuario(String user, String pass)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            var result = "n";
            MySqlConnection conexion = new MySqlConnection(ruta);
            conexion.Open();

            MySqlCommand command = conexion.CreateCommand();
            command.CommandText = ("INSERT INTO usuarios (`user`, `password`) " +
                                    "VALUES ('" + user + "', '" + pass + "');");
            command.Connection = conexion;

            try
            {
                command.ExecuteNonQuery();


                //Context.Response.Write(result = "Insert Exitoso");
                //command.Connection.Close();

            }
            catch (Exception e)
            {

                Context.Response.Write(result = "Error al Crear Usuario");
            }

            Context.Response.Write(result = "Usuario Creado");
            command.Connection.Close();

        }






    }
}
